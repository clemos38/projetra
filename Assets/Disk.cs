using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disk : MonoBehaviour
{
	public int size; //0 plus petit, 1 moyen, 2 plus grand
	public bool ontop; //true si c'est le disque le plus en haut de sa tour
	public int tower; //numéro de sa tour : 0 à 2 de gauche à droite 
	public int position_on_tower; //0 sur la plateau, 1 au milieu, 2 au dessus des deux autres disques
	
	float speed = 1f;

	// Start is called before the first frame update
    void Start()
    {
		tower = 0;
        if(size == 2){
			ontop = false;
			position_on_tower = 0;
		}
		if(size == 1){
			ontop = false;
			position_on_tower = 1;
		}
		if(size == 0){
			ontop = true;
			position_on_tower = 2;
		}
    }

    // Update is called once per frame
    void Update()
    {
	    
    }
}
