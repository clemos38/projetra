using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class PlaceCube : MonoBehaviour
{
    public ARRaycastManager aRSessionOrigin;
    public GameObject prefabToPlace;

    public List<ARRaycastHit> hits;
    public Camera m_firstPersonCamera;
	bool is_put = false;

	private GameObject AR;
	private GameObject ARCamera;
	private GameObject instructions;

	void Start()
    {

        hits = new List<ARRaycastHit>();

        aRSessionOrigin = GetComponent<ARRaycastManager>();
        
        AR = GameObject.FindGameObjectWithTag("AR");
        ARCamera = GameObject.FindGameObjectWithTag("MainCamera");
        instructions = GameObject.FindGameObjectWithTag("instructions");

    }



    void Update()
    {

         if (Input.touchCount > 0 && is_put == false)
        {
            Touch touch = Input.GetTouch(0);
            Ray raycast = m_firstPersonCamera.ScreenPointToRay(touch.position);
            RaycastHit raycastHit;
            if (Physics.Raycast(raycast, out raycastHit))
            {
                    Vector3 position = raycastHit.point;
					GameObject Cube;
					Vector3 dir = m_firstPersonCamera.transform.position - position;
					dir.y = 0; // keep the direction strictly horizontal
					Quaternion rot = Quaternion.LookRotation(dir);

					
					//ARCamera.transform.rotation = Quaternion.Inverse(rot);
					
					
					
                    Cube = Instantiate(prefabToPlace, position, rot);

                    is_put = true;
					
					//AR.transform.rotation = rot;
					
					
					// deletes the planes
					foreach (var plane in AR.GetComponent<ARPlaneManager>().trackables)
						plane.gameObject.SetActive(false);
					
					
					// disables the plane manager
					//gameObject.SetActive(false);
					AR.GetComponent<ARPlaneManager>().enabled = false;
					
					// Updates the instructions
					instructions.GetComponent<Text>().text = "Drag and drop the disks on the towers !";


            }
		}
	}
}