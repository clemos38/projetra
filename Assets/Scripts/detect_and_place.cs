using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class detect_and_place : MonoBehaviour
{
    
    // Current tower of the disk
    [SerializeField] public int tower = -1;
    
    // Place of the disk on the tower
    [SerializeField] private int place_on_tower = 0;
    
    // Size of the disk
    [SerializeField] private int size = 1;
    
    
    // List of all the towers
    private GameObject[] towers;
    
    // List of all the disks
    private GameObject[] disks;
    
    // Table sur laquelle placer les disques
    private GameObject table;

    // Tells if the object has been moved before replacing it
    public bool has_moved = false;
    
    // Sound played whenever a disk is placed on a tower
    private GameObject wood_sound;
    
    // Sound played when the game is won
    private GameObject victory_sound;
    
    // In order to count the moves
    private GameObject manager;
    
    // Particle effect for when the game is won
    private GameObject victory_effect;
    
    // Instructions text
    private GameObject instructions;
    
    
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        
        // Creates the list of all the towers in the scene (all the objects tagged with "tower")
        towers = GameObject.FindGameObjectsWithTag("tower");
        
        // Gets the right sound for the disks
        wood_sound = GameObject.FindGameObjectWithTag("woodSound");
        
        // Gets the right sound for the game end
        victory_sound = GameObject.FindGameObjectWithTag("victorySound");
        
        // Gets the manager
        manager = GameObject.FindGameObjectWithTag("manager");
        
        // Gets the particle emitter
        victory_effect = GameObject.FindGameObjectWithTag("endEffect");
        
        // Gets the text for the instructions
        instructions = GameObject.FindGameObjectWithTag("instructions");

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            PlaceDisk();
            Debug.Log("Replaced");
        }

    }
    
    
    // Function that checks to which tower the object is the closest to, and returns its index in the list of towers
    int DetectClosestTower()
    {
        
        double d_min = double.PositiveInfinity;

        double d;
        int index = 0;

        // Calculates the index which provides the smallest distance between the object and a tower
        for (int i = 0; i < towers.Length; i++)
        {
            d = Vector3.Distance(gameObject.transform.position, towers[i].transform.position);
            if (d < d_min)
            {
                d_min = d;
                index = i;
            }

        }
        
        return index;
    }
    
    
    
    // Function which replaces the object at the position of a given tower
    void PlaceToTower(int i)
    {
        
        // Creates the list of all the disks in the scene (all the objects tagged with "disk")
        disks = GameObject.FindGameObjectsWithTag("disk");
        int max_place = -1;
        
        // Size of the top disk
        int top_size = size + 1;
        
        foreach (GameObject disk in disks)
        {
            
            // If the disk is not itself
            if (!disk.Equals(gameObject))
                // If the disk is on this stack and its place is so far the highest
                if (disk.GetComponent<detect_and_place>().tower == i && disk.GetComponent<detect_and_place>().place_on_tower > max_place)
                {
                    // Updates the maximum place on this stack
                    max_place = disk.GetComponent<detect_and_place>().place_on_tower;
                    top_size = disk.GetComponent<detect_and_place>().size;
                }
        }
        
        // Checks if the disk on the top of this i stack is bigger
        // if yes then places the disk on it
        // otherwise, places it back on its previous stack
        if (size <= top_size)
        {
            // Puts the disk on the top of the stack
            place_on_tower = max_place + 1;
        
        
            // Gets the table on which the disk will be placed
            table = GameObject.FindGameObjectWithTag("table");
        
            float table_height = 0.025f;
            float disk_height = 0.05f;
        
            // Places the disk on the right spot
            transform.position = new Vector3(towers[i].transform.position.x, table.transform.position.y + table_height + ((float)place_on_tower + 0.5f) * disk_height, towers[i].transform.position.z);

            // If the object has been placed on a new stack, increments the move counter
            if (i != tower && manager.GetComponent<PlayerManager>().begin && !manager.GetComponent<PlayerManager>().end)
                manager.GetComponent<PlayerManager>().moves_count += 1;
            
            // May change the color of the outline of the disk
            if (i == 1)
            {
                gameObject.GetComponent<cakeslice.Outline>().color = 2;
            }
            else
            {
                gameObject.GetComponent<cakeslice.Outline>().color = 1;
            }
                
            
            // Updates on which tower the disk is
            tower = i;
        
        
            has_moved = false;
            
            wood_sound.GetComponent<AudioSource>().Play();
            
            
            // If by this move the player has won the game
            if (GameWon())
            {
                // Updates the instructions
                
                victory_effect.GetComponent<ParticleSystem>().Play();
                
                if (!manager.GetComponent<PlayerManager>().end) {
                    //manager.GetComponent<PlayerManager>().moves_count += 1;
                    victory_sound.GetComponent<AudioSource>().Play();
                    manager.GetComponent<PlayerManager>().end = true;
                    manager.GetComponent<PlayerManager>().time_spent = Time.time - manager.GetComponent<PlayerManager>().begin_time;
                    manager.GetComponent<PlayerManager>().score = (int) (100000 / manager.GetComponent<PlayerManager>().moves_count / manager.GetComponent<PlayerManager>().time_spent);
                    instructions.GetComponent<Text>().text = "You won !\n\nScore : " + manager.GetComponent<PlayerManager>().score;
                }
            }
            
        }

        else
        {
            PlaceToTower(tower);
        }
        
        
        
        
    }
    
    
    // Function which replaces the object on the closest tower
    void PlaceDisk()
    {
        PlaceToTower(DetectClosestTower());
        
    }
    
    
    // When the object is not touched anymore
    void OnMouseUp() 
    {
        if (has_moved)
            PlaceDisk();
    }
    
    
    // Function which returns if the object is on the top of its stack
    public bool IsOnTop()
    {
        disks = GameObject.FindGameObjectsWithTag("disk");
        int max_place = -1;
        foreach (GameObject disk in disks)
        {
            
            // If the disk is not itself
            if (!disk.Equals(gameObject))
                // If the disk is on this stack and its place is so far the highest
                if (disk.GetComponent<detect_and_place>().tower == tower && disk.GetComponent<detect_and_place>().place_on_tower > max_place)
                {
                    // Updates the maximum place on this stack
                    max_place = disk.GetComponent<detect_and_place>().place_on_tower;
                }
        }

        return (place_on_tower >= max_place);

    }
    
    
    // Function which returns if the game is won
    bool GameWon()
    {
        disks = GameObject.FindGameObjectsWithTag("disk");
        foreach (GameObject disk in disks)
        {
            if (disk.GetComponent<detect_and_place>().tower != 1)
                return false;
        }

        return true;
    }
    
}
