using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using System;


public class PlayerManager : MonoBehaviour
{
    private Vector3 currentTouchPosition;
    private Vector3 previousTouchPosition;
    private float speed = 0.025f;
    //[SerializeField] private ARRaycastManager m_RaycastManager;
    private GameObject childObject;
    //public enum HoverState { HOVER, NONE };
    //public HoverState hover_state = HoverState.NONE;

    public Camera m_firstPersonCamera;

    private GameObject hitObj;
    private Vector3 position;
    private float width;
    private float height;
    
    private GameObject movingObject;
    private bool is_moving = false;

    public int moves_count = 0;

    private GameObject ARCamera;


    public bool begin = false;
    public float begin_time;
    private GameObject timer;
    public bool end = false;
    public float time_spent;

    public int score;

    
    // Move counter text
    private GameObject move_count_text;
    
    private void Awake()
    {
        width = (float)Screen.width / 2.0f;
        height = (float)Screen.height / 2.0f;
    }

    private void Start()
    {
        ARCamera = GameObject.FindGameObjectWithTag("MainCamera");
        
        // Gets the text for the counter of moves played
        move_count_text = GameObject.FindGameObjectWithTag("moveCount");
        
        // Gets the text to display the time spent
        timer = GameObject.FindGameObjectWithTag("timer");
    }

    private void FixedUpdate()
    {
        // Texts updates
        if (end)
            timer.GetComponent<Text>().text = "final time : " + Math.Round(time_spent, 2) + "s";
        else if (begin)
        {
            timer.GetComponent<Text>().text = "time : " + Math.Round((Time.time - begin_time), 2) + "s";
        }
        
        if (begin)
            move_count_text.GetComponent<Text>().text = "move count : " + moves_count;
            
        
        
        //RAYCAST QUI MARCHE
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            // Store first touch position on start
            previousTouchPosition = Input.GetTouch(0).deltaPosition;
            Ray raycast = m_firstPersonCamera.ScreenPointToRay(touch.position);
            RaycastHit raycastHit;
            if (Physics.Raycast(raycast, out raycastHit))
            {
                //Debug.LogError("Something Hit");
                if (raycastHit.transform.tag == "disk" && !is_moving)
                {
                    if (raycastHit.collider.gameObject.GetComponent<detect_and_place>().tower == -1 || raycastHit.collider.gameObject.GetComponent<detect_and_place>().IsOnTop())
                    {
                        is_moving = true;
                        if (!begin)
                        {
                            begin = true;
                            begin_time = Time.time;
                        }
                        movingObject = raycastHit.collider.gameObject;
                        hitObj = raycastHit.collider.gameObject;
                        movingObject.GetComponent<detect_and_place>().has_moved = true;
                        gameObject.GetComponent<cakeslice.Outline>().color = 0;
                    }
                    
                }

                if (is_moving)
                {
                    if (touch.phase == TouchPhase.Moved)
                    {
                        float step = speed * Time.deltaTime * Vector3.Distance(ARCamera.transform.position, movingObject.transform.position); // calculates the distance to move
                        movingObject.transform.position = movingObject.transform.position + Vector3.Dot(movingObject.transform.right, ARCamera.transform.right) * movingObject.transform.right * touch.deltaPosition.x * step;
                        movingObject.transform.position = movingObject.transform.position + Vector3.Dot(movingObject.transform.forward, ARCamera.transform.forward) * movingObject.transform.forward * touch.deltaPosition.y * step;
                        
                    }
                }
                
                Debug.LogError("Something Hit");
                if (raycastHit.transform.tag == "gameObjectCollider")
                {
                    hitObj = raycastHit.collider.gameObject;
                    Debug.LogError("andy clicked");
                    if (touch.phase == TouchPhase.Moved)
                    {
                        Vector3 destination = Input.GetTouch(0).deltaPosition;
                        //destination.x = (destination.x - width) / width;
                        //destination.y = (destination.y - height) / height;
                        //destination.z = hitObj.transform.position.z;
                        float step = speed * Time.deltaTime; // calculate distance to move
                        //hitObj.transform.position = m_firstPersonCamera.ScreenToWorldPoint(new Vector3 (touch.position.x, touch.position.y, hitObj.transform.position.z));
                        hitObj.transform.position = Vector3.Lerp(hitObj.transform.position, destination, step);
                    }
                }
            }
        }
        else
            {
                is_moving = false;
            }
        }
}
