﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{


    public string Scene;



    public void GoToScene() {
        SceneManager.LoadScene(Scene, LoadSceneMode.Single);
    }


}
